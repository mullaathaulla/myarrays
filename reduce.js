function reduce(elements,cb,startingValue){
    if(startingValue===null || startingValue===undefined){
        startingValue=elements[0];
        for(let i=1;i<elements.length;i++){
            startingValue=cb(startingValue,elements[i]);
        }
    }
    else{
        for(let x of elements){
            startingValue=cb(startingValue,x);
        }
    }
    return startingValue;

}

module.exports=reduce;