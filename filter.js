function filter(elements,cb){
    let arrayOfFindedElements=[];
    for(let i=0;i<elements.length;i++){
        if(cb(elements[i])){
            arrayOfFindedElements.push(elements[i]);
        }
    }
    return arrayOfFindedElements;

}

module.exports=filter;